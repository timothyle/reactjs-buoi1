import React, { Component } from 'react'
import HeadersLay from './HeadersLay'
import Body1 from './Body1'
import Footer1 from './Footer1'


export default class BaiTapLayout extends Component {
  render() {
    return (
      <div>
      <HeadersLay/>
      <Body1/>
      <Footer1/>
      </div>
    )
  }
}
